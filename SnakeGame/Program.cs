﻿namespace SnakeGame
{
    struct Board
    {
        public static readonly int gridWidth = 8;
        public static readonly int gridHeight = 12;
        public static char[,] gameGrid = new char[gridWidth, gridHeight];
        private Random random = new();

        public Board(int width = 90, int height = 35)
        {
            Width = width;
            Height = height;
            // gridHeight = new Cell[Width, Height];
        }

        private int leftEdgeX => 0;
        private int rightEdgeX => 0;
        private int bottomEdgeY => Height - 1;

        public Snake Snake { get; private set; }
        public int Height { get; private set; }
        public int Width { get; private set; }
        public Cell Current => Snake.Head;
        public Cell Center => get(gridWidth / 2, Height / 2);

        static void Set3DArraySize(int sizex, int sizey)
        {
            for (int y = 0; y < sizey; y++)
            {
                for (var x = 0; x < sizex; x++)
                {
                    gameGrid[x, y] = ' ';
                }
            }
        }

        public struct Cell
        {
        }

        static void PrintGrid(char[,] grid)
        {
            Console.WriteLine("+" + new string('-', gridWidth) + "+");
            for (int y = 0; y < gridHeight; y += 1)
            {
                Console.WriteLine("|");
                for (int x = 0; x < gridWidth; x += 1)
                {
                    Console.Write(grid[x, y]);
                }
                Console.WriteLine("|");
            }
            Console.WriteLine();
        }

        

        static void GameLoop()
        {
            var userGameEvent = Console.ReadKey().KeyChar;
            while (userGameEvent != 'x')
            {

                switch (userGameEvent)
                {
                    default:
                        break;
                }
            }

            Console.WriteLine("Hope you had fun :)");
            Environment.Exit(0);
        }

        
    }

    struct Snake
    {
        public int xPosition;
        public int yPosition;
        LinkedList<char> body;
    
        public struct BodySegment;
        public LinkedListNode<BodySegment> Head;

        public enum Direction {
            UP,
            DOWN,
            LEFT,
            RIGHT
        }

        public void StartDirectionalEventListener()
        {
            while (true)
            {
                var userKeyPress = Console.ReadKey().KeyChar;
                switch (userKeyPress)
                {
                    case 'k': { break; /* UP */}
                    case 'j': { break; /* DOWN */}
                    case 'h': { break; /* LEFT */}
                    case 'l': { break; /* RIGHT */}
                }
            }
        }
    }

    class SnakeGame {

        public static async Task Main()
        {
            Board board = new(90, 35);

            Snake snake = new();
            snake.StartDirectionalEventListener();
            
            Console.WriteLine("Sucks ass bruh");
            Console.WriteLine("Welcome to my shitty snake game");
        }
    }

}