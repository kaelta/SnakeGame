# Architecture of the game

It should be straightforward, right?
Honestly the hardeset part for me right now is just learning C# syntax, but the rest is pretty understandable.

                        Program
                            |
                            |
                Board  ---------- Snake
                (init)            (init)
                    |               |
                    |               |
                    ---------- update position
                    |            on tick,
                    |        register keypress
                    |               |
                    |               |
                    |          ate self? die!
                    |
                    add food ----
                    |           |
                    |           |
                    is eaten? - yes

```csharp
class Board {}

class Snake {
    LinkedList body
    LinkedListNode Head
}

class Food {}

class Program {
    

    start() {
        new Board();
        new Snake();

        snake.ListenForKeypresses();

        while (!gameNotOver) {
            UpdateGameTick();
            RenderGame();
            await Task.Delay(tickRate);
        }
    }
}
```